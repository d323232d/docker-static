FROM nginx

COPY ./assets /usr/share/nginx/html
COPY ./css /usr/share/nginx/html
COPY ./index.html /usr/share/nginx/html

EXPOSE 80